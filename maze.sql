-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 09, 2014 at 05:16 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maze`
--
CREATE DATABASE IF NOT EXISTS `maze` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `maze`;

-- --------------------------------------------------------

--
-- Table structure for table `attending_question`
--

CREATE TABLE IF NOT EXISTS `attending_question` (
  `email` varchar(35) NOT NULL,
  `attending` int(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attending_question`
--

INSERT INTO `attending_question` (`email`, `attending`) VALUES
('b@b', 0);

-- --------------------------------------------------------

--
-- Table structure for table `breakers`
--

CREATE TABLE IF NOT EXISTS `breakers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(400) NOT NULL,
  `answer` varchar(100) NOT NULL,
  `imgsrc` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `breakers`
--

INSERT INTO `breakers` (`id`, `question`, `answer`, `imgsrc`) VALUES
(1, '1.	There is a completely sealed glass box as shown, completely filled with water, except for a single air bubble. The box is placed in a bus, which started suddenly with a jerk. Now, which side will the bubble move towards, A or B? ', 'b', '1.png'),
(2, '2.	How many triangles are there in total in the picture depicted?', 'nineteen', '2.png\r\n'),
(3, '3.	A man builds a house rectangular in shape. All sides have southern exposure. A big bear walks by, what color is the bear', 'white', '3.png');

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE IF NOT EXISTS `marks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mark` int(10) NOT NULL,
  `mobileno` varchar(10) NOT NULL,
  `emailid` varchar(35) NOT NULL,
  `allow` int(1) NOT NULL,
  `time` varchar(100) NOT NULL,
  `help` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `marks`
--

INSERT INTO `marks` (`id`, `mark`, `mobileno`, `emailid`, `allow`, `time`, `help`) VALUES
(20, 0, '1', 'p@p', 0, '2014,10,7,19,101,12,648', 0),
(21, 0, '1', 'a@q', 1, '2014,10,8,18,112,22,385', 2),
(22, 0, '12', 's@a', 1, '2014,10,8,19,113,18,892', 2);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(400) NOT NULL,
  `answer` varchar(100) NOT NULL,
  `imgsrc` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `answer`, `imgsrc`) VALUES
(1, '1.There is a completely sealed glass box as shown, completely filled with water, except for a single air bubble. The box is placed in a bus, which started suddenly with a jerk. Now, which side will the bubble move towards, A or B?', '10', '1.png'),
(2, '2.How many triangles are there in total in the picture depicted?', 'nineteen', '2.png'),
(3, '3.A man builds a house rectangular in shape. All sides have southern exposure. A big bear walks by, what color is the bear?', 'white', '3.png'),
(4, '4.A doctor gives you three pills telling you to take one every half hour. How long would the pills last?', 'onehour', '4.png'),
(5, '5.There''s one sport in which neither the spectators nor the participants know the score or the leader until the contest ends. What is it? ', 'boxing', '5.png'),
(6, '6', 'holes', '6.png'),
(7, '7', '31-12', '7.png'),
(9, '9', 'mother', '9.png'),
(10, '10', 'parachute', '10.png'),
(11, '11', 'no', '11.png'),
(12, '12', 'wednesday', '12.png'),
(13, '13', 'a', '13.png'),
(14, '14', '120', '14.png'),
(15, '15', 'b', '15.png'),
(16, '16', 'seven', '16.png'),
(17, '17', 'zero', '17.png'),
(18, '18', 'nikki', '18.png');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(35) NOT NULL,
  `name` varchar(35) NOT NULL,
  `mobileno` varchar(10) NOT NULL,
  `teamname` varchar(20) NOT NULL DEFAULT 'Gabbar',
  `attempt` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `mobileno`, `teamname`, `attempt`) VALUES
(25, 'p@p', 'p', '1', 'p', 1),
(26, 'pp@ymail.com', 'Rockstars', '12', 'Rockstars', 1),
(27, 'b@b', 'b', '2', 'b', 1),
(28, 'a@q', 'q', '1', 'q', 2),
(29, 's@a', 'dakku', '12', 'dakku', 2),
(30, 'a@w', 'asaphalt', '12', 'asaphalt', 0),
(31, 'w@w', 'rcfvg', '1', 'rcfvg', 0),
(32, 'k@d', 'keertan', '1', 'keertan', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
