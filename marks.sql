-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2014 at 01:57 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maze`
--

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE IF NOT EXISTS `marks` (
`id` int(10) NOT NULL,
  `mark` int(10) NOT NULL,
  `mobileno` varchar(10) NOT NULL,
  `emailid` varchar(35) NOT NULL,
  `allow` int(1) NOT NULL,
  `time` varchar(100) NOT NULL,
  `help` int(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `marks`
--

INSERT INTO `marks` (`id`, `mark`, `mobileno`, `emailid`, `allow`, `time`, `help`) VALUES
(20, 1, '1', 'p@p', 0, '2014,10,9,11,92,26,872', 0),
(21, 0, '1', 'a@q', 1, '2014,10,8,18,112,22,385', 2),
(22, 0, '12', 's@a', 1, '2014,10,8,19,113,18,892', 2),
(23, 8, '1', 's@w', 1, '2014,10,9,12,104,57,57', 0),
(24, 0, '9791143425', 'minnie2295@gmail.com', 1, '2014,10,9,15,79,18,149', 2),
(25, 0, '1', 'sam@gmail.com', 1, '2014,10,9,15,76,30,889', 2),
(26, 0, '9566064064', 'an@gmail.com', 0, '2014,10,9,15,91,21,909', 2),
(27, 8, '9941244783', 'ty@gmail.com', 1, '2014,10,9,15,101,12,842', 0),
(28, 1, '9884161084', 'a@gmail.com', 1, '2014,10,9,15,102,26,150', 0),
(29, 11, '9791143425', 'm@gmail.com', 1, '2014,10,9,16,64,15,753', 0),
(30, 5, '9962414989', 'sa@gmail.com', 1, '2014,10,9,15,88,21,860', 0),
(31, 7, '8681916281', 'p@gmail.com', 1, '2014,10,9,15,85,43,206', 0),
(32, 6, '8438541621', 'ta@gmail.com', 1, '2014,10,9,15,89,49,421', 0),
(33, 12, '9790879533', 'ch@gmail.com', 1, '2014,10,9,15,90,21,184', 0),
(34, 4, '9677357277', 's@gmail.com', 1, '2014,10,9,15,93,19,195', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `marks`
--
ALTER TABLE `marks`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `marks`
--
ALTER TABLE `marks`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
